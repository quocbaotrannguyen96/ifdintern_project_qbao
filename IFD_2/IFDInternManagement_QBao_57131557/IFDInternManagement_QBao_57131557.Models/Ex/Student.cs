﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFDInternManagement_QBao_57131557.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class Student
    {
        public string ConfirmPassword { get; set; }
    }
    public class UserMetadata
    {
        [Display(Name = "Mã sinh viên")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string StudentID { get; set; }

        [Display(Name = "Họ và Tên")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string Studentname { get; set; }

        [Display(Name ="Ngày sinh")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Birthday { get; set; }

        [Display(Name ="Địa chỉ")]
        [Required(AllowEmptyStrings = false, ErrorMessage ="Không được bỏ trống!")]
        public string Address { get; set; }

        [Display(Name ="Số điện thoại")]
        [Required(AllowEmptyStrings = false, ErrorMessage ="Không được bỏ trống!")]
        [DataType(DataType.PhoneNumber)]
        public string Phonenumber { get; set; }

        [Display(Name ="Trường học")]
        [Required(AllowEmptyStrings =false, ErrorMessage ="Không được bỏ trống!")]
        public string Unit { get; set; }

        [Display(Name ="Giới thiệu")]
        [Required(AllowEmptyStrings = false, ErrorMessage ="Không được bỏ trống!")]
        public string Intro { get; set; }
                
        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Mật khẩu")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Tối thiểu 6 ký tự!")]
        public string Password { get; set; }

        [Display(Name = "Xác nhận mật khẩu")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Không trùng khớp!")]
        public string ConfirmPassword { get; set; }

        [Display(Name ="Ngôn ngữ lập trình")]
        [Required(AllowEmptyStrings = false, ErrorMessage ="Không được bỏ trống!")]
        public int SpecializeID { get; set; }

    }
}
