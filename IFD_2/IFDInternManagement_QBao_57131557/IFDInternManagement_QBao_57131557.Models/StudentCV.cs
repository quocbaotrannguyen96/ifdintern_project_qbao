﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class StudentCV
    {
        public int ID { get; set; }
        [Display(Name = "Mã sinh viên")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string StudentID { get; set; }
        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string Email { get; set; }
        [Display(Name = "Biết công ty chúng tôi qua")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public int KnowUsID { get; set; }
        [Display(Name = "Framework đã từng sử dụng")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string FrameworkUse { get; set; }
        [Display(Name = "Khó khăn, thuận lợi khi làm việc nhóm không quen trước đó")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string Problem { get; set; }
        [Display(Name = "Mô tả tóm lược một project sinh viên đã làm độc lập")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string ExAppAlone { get; set; }
        [Display(Name = "Ngôn ngữ lập trình ưa thích")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public int SpecializeID { get; set; }
        [Display(Name = "Giới thiệu vài nét về bản thân")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string Intro { get; set; }
        [Display(Name = "Vị trí thực tập mong đợi")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public int ExpectedLocationID { get; set; }
        [Display(Name = "Thời gian bắt đầu thực tập")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime InternStartDate { get; set; }
        [Display(Name = "Mô tả tóm lược một project sinh viên đã làm với nhóm")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string ExAppTeam { get; set; }
        public bool Status { get; set; }

        [NotMapped]
        public List<KnowU> KnowUCollection { get; set; }
        [NotMapped]
        public List<ExpectedLocation> ExpectedCollection { get; set; }
        [NotMapped]
        public List<Specialize> SpecializeCollection { get; set; }

        public virtual ExpectedLocation ExpectedLocation { get; set; }
        public virtual KnowU KnowU { get; set; }        
        public virtual Specialize Specialize { get; set; }
    }
}
