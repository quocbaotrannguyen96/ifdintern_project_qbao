//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Trainingprogram
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Trainingprogram()
        {
            this.Internships = new HashSet<Internship>();
            this.TrainingDetails = new HashSet<TrainingDetail>();
        }
    
        public int ID { get; set; }
        public string TrainingprogramID { get; set; }
        public string Trainingprogramname { get; set; }
        public System.DateTime Startdate { get; set; }
        public System.DateTime Enddate { get; set; }
        public string Trainingcontent { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Internship> Internships { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TrainingDetail> TrainingDetails { get; set; }
    }
}
