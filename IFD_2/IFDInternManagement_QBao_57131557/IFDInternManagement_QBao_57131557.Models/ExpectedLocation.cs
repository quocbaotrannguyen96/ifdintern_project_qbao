//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExpectedLocation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ExpectedLocation()
        {
            this.StudentCVs = new HashSet<StudentCV>();
        }
    
        public int ExpectedLocationID { get; set; }
        public string ExpectedLocationName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentCV> StudentCVs { get; set; }
    }
}
