﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.IService
{
    public interface IProjectManagementService
    {
        Project GetUserById(int id);
        IEnumerable<Project> GetAllUsers();
        int CreateUser(Project userEntity);
        bool UpdateUser(int id, Project userEntity);
        bool DeleteUser(int id);
    }
}
