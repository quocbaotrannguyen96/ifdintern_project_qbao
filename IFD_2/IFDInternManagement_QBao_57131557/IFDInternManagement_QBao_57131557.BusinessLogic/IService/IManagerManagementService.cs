﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.IService
{
    public interface IManagerManagementService
    {
        Manager GetUserById(int id);
        IEnumerable<Manager> GetAllUsers();
        int CreateUser(Manager userEntity);
        bool UpdateUser(int id, Manager userEntity);
        bool DeleteUser(int id);
    }
}
