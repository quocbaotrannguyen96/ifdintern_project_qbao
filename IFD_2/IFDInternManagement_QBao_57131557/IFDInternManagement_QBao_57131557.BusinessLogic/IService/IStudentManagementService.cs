﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.IService
{
    public interface IStudentManagementService
    {
        Student GetUserById(string id);
        IEnumerable<Student> GetAllUsers();
        int CreateUser(Student userEntity);
        bool UpdateUser(string id, Student userEntity);
        bool DeleteUser(string id);
    }
}
