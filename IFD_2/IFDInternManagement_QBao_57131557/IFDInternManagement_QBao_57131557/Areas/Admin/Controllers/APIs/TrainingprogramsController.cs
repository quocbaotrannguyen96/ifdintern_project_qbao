﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.Areas.Admin.Controllers.APIs
{
    public class TrainingprogramsController : ApiController
    {
        private IFDInternsEntities db = new IFDInternsEntities();

        // GET: api/Trainingprograms
        public IQueryable<Trainingprogram> GetTrainingprograms()
        {
            return db.Trainingprograms;
        }

        // GET: api/Trainingprograms/5
        [ResponseType(typeof(Trainingprogram))]
        public IHttpActionResult GetTrainingprogram(string id)
        {
            Trainingprogram trainingprogram = db.Trainingprograms.Find(id);
            if (trainingprogram == null)
            {
                return NotFound();
            }

            return Ok(trainingprogram);
        }

        // PUT: api/Trainingprograms/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTrainingprogram(string id, Trainingprogram trainingprogram)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != trainingprogram.TrainingprogramID)
            {
                return BadRequest();
            }

            db.Entry(trainingprogram).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrainingprogramExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Trainingprograms
        [ResponseType(typeof(Trainingprogram))]
        public IHttpActionResult PostTrainingprogram(Trainingprogram trainingprogram)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Trainingprograms.Add(trainingprogram);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TrainingprogramExists(trainingprogram.TrainingprogramID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = trainingprogram.TrainingprogramID }, trainingprogram);
        }

        // DELETE: api/Trainingprograms/5
        [ResponseType(typeof(Trainingprogram))]
        public IHttpActionResult DeleteTrainingprogram(string id)
        {
            Trainingprogram trainingprogram = db.Trainingprograms.Find(id);
            if (trainingprogram == null)
            {
                return NotFound();
            }

            db.Trainingprograms.Remove(trainingprogram);
            db.SaveChanges();

            return Ok(trainingprogram);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TrainingprogramExists(string id)
        {
            return db.Trainingprograms.Count(e => e.TrainingprogramID == id) > 0;
        }
    }
}