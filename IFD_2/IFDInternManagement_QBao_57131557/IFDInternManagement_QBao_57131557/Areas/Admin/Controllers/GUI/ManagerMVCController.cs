﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IFDInternManagement_QBao_57131557.Models;
using Newtonsoft.Json;
using PagedList;

namespace IFDInternManagement_QBao_57131557.Areas.Admin.Controllers.GUI 
{
    //[Authorize(Roles = "Admin")]
    public class ManagerMVCController : Controller
    {
        // GET: ManagerMVC
        private IFDInternsEntities db = new IFDInternsEntities();
        string url = "http://localhost:49517/api/manager";
        HttpClient client;
        public ManagerMVCController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // GET: mvcusers
        //public async Task<ActionResult> Index()
        //{
        //    HttpResponseMessage responseMessage = await client.GetAsync(url);
        //    if (responseMessage.IsSuccessStatusCode)
        //    {
        //        var responseData = responseMessage.Content.ReadAsStringAsync().Result;

        //        var usrs = JsonConvert.DeserializeObject<List<Manager>>(responseData);

        //        return View(usrs);
        //    }
        //    return View("Error");
        //}
        public ActionResult Index(int? page)
        {
            if (page == null) page = 1;

            var managers = (from l in db.Managers
                            select l).OrderBy(x => x.ID);
            int pageSize = 5;

            int pageNumber = (page ?? 1);
            return View(managers.ToPagedList(pageNumber, pageSize));
        }
            // GET: mvcusers/Details/5
        public async Task<ActionResult> Details(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                var usr = JsonConvert.DeserializeObject<Manager>(responseData);
                return View(usr);
            }
            return View("Error");
        }
        // GET: mvcusers/Create
        public ActionResult Create()
        {
            Manager manager = new Manager();
            manager.RoleCollection = db.Roles.ToList<Role>();
            manager.ProjectCollection = db.Projects.ToList<Project>();
            return View(manager);
        }

        // POST: mvcusers/Create

        [HttpPost]
        public async Task<ActionResult> Create(Manager usr)
        {
            
            HttpResponseMessage responseMessage = await client.PostAsJsonAsync(url, usr);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Error");
        }


        // GET: mvcusers/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            //Manager manager = new Manager();
            
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var manager = JsonConvert.DeserializeObject<Manager>(responseData);
                manager.RoleCollection = db.Roles.ToList<Role>();
                manager.ProjectCollection = db.Projects.ToList<Project>();
                return View(manager);
            }
            return View("Error");
        }

        // POST: mvcusers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<ActionResult> Edit(int id, Manager usr)
        {

            HttpResponseMessage responseMessage = await client.PutAsJsonAsync(url + "/" + id, usr);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Error");
        }

        public async Task<ActionResult> Delete(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var usr = JsonConvert.DeserializeObject<Manager>(responseData);

                return View(usr);
            }
            return View("Error");
        }
        //The DELETE method
        [HttpPost]
        public async Task<ActionResult> Delete(int id, Manager usr)
        {

            HttpResponseMessage responseMessage = await client.DeleteAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Error");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
