﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.Controllers
{
    public class StudentCVController : Controller
    {
        // GET: StudentCV
        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegistrationCV([Bind(Exclude = "Status")] StudentCV studentcv)
        {
            bool Status = false;
            string message = "";
            if (ModelState.IsValid)
            {
                studentcv.Status = false;
                //Save to Database
                using (IFDInternsEntities dc = new IFDInternsEntities())
                {
                    dc.StudentCVs.Add(studentcv);
                    dc.SaveChanges();
                    //Send Email to User
                    SendVerificationLinkEmail(studentcv.Email);
                    message = "Đã gửi CV thành công!" +
                        " thông báo đã được gửi đến địa chỉ email:" + studentcv.Email;
                    Status = true;
                }
            }
            else
            {
                message = "Không hợp lệ!";
            }
            ViewBag.Message = message;
            ViewBag.Status = Status;
            return View(studentcv);
        }
        [NonAction]
        public void SendVerificationLinkEmail(string email, string emailFor = "VerifyAccount")
        {
            var fromEmail = new MailAddress("mariobolobala@gmail.com", "mario bolobala");
            var toEmail = new MailAddress(email);
            var fromEmailPassword = "tranhuutich1";

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Chúng tôi đã nhận được CV của bạn!";
                body = "<br/><br/>Chúng tôi vui lòng thông báo CV của bạn" +
                    " đã được chúng tôi tiếp nhận. Vui lòng đợi quá trình xử lý hoàn tất!";
                
            }
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
    }
}