﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using IFDInternManagement_QBao_57131557.BusinessLogic.IService;
using IFDInternManagement_QBao_57131557.BusinessLogic.Service;
using IFDInternManagement_QBao_57131557.Models;
using IFDInternManagement_QBao_57131557.Models.Repository;
using Newtonsoft.Json.Serialization;
using Unity;
using Unity.Lifetime;

namespace IFDInternManagement_QBao_57131557
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();
            container.RegisterType<ISpecializeManagementService, SpecializeManagementService>(new HierarchicalLifetimeManager());
            container.RegisterType<IStudentManagementService, StudentManagementService>(new HierarchicalLifetimeManager());
            container.RegisterType<IManagerManagementService, ManagerManagementService>(new HierarchicalLifetimeManager());
            container.RegisterType<IProjectManagementService, ProjectManagementService>(new HierarchicalLifetimeManager());
            container.RegisterType<IStudentCVManagementService, StudentCVManagementService>(new HierarchicalLifetimeManager());
            //container.RegisterType<IRepository<Specialize>, Repository<Specialize>>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            //var json = config.Formatters.JsonFormatter;
            //json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            //config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new System.Net.Http.Headers.MediaTypeHeaderValue("application/json"));

            //var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            //jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
           
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
