﻿using System.Web;
using System.Web.Mvc;

namespace IFDInternManagement_QBao_57131557
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
