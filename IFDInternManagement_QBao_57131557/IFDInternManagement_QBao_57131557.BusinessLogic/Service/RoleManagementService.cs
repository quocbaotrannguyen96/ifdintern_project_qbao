﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using IFDInternManagement_QBao_57131557.Models;
using IFDInternManagement_QBao_57131557.Models.Repository;
using IFDInternManagement_QBao_57131557.Models.ViewModel;
using IFDInternManagement_QBao_57131557.BusinessLogic.IService;
using IFDInternManagement_QBao_57131557.Models.UnitOfWork;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.Service
{
    public class RoleManagementService : IRoleManagementService
    {
        private readonly UnitOfWorkRole _unitOfWork;
        public RoleManagementService()
        {
            _unitOfWork = new UnitOfWorkRole();
        }
        public Role GetUserById(int id)
        {
            return _unitOfWork.UserRepository.GetByID(id);

        }
        public IEnumerable<Role> GetAllUsers()
        {
            return _unitOfWork.UserRepository.GetAll().ToList();
        }
        public int CreateUser(Role userEntity)
        {
            _unitOfWork.UserRepository.Insert(userEntity);
            _unitOfWork.Save();
            return userEntity.RoleID;

        }
        public bool UpdateUser(int id, Role userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.UserRepository.GetByID(id);
                    if (usr != null)
                    {                        
                        usr.Rolename = userEntity.Rolename;
                        _unitOfWork.UserRepository.Update(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public bool DeleteUser(int id)
        {
            var success = false;
            if (id > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.UserRepository.GetByID(id);
                    if (usr != null)
                    {

                        _unitOfWork.UserRepository.Delete(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}

