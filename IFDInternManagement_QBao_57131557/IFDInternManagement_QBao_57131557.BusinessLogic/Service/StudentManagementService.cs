﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using IFDInternManagement_QBao_57131557.Models;
using IFDInternManagement_QBao_57131557.Models.Repository;
using IFDInternManagement_QBao_57131557.Models.ViewModel;
using IFDInternManagement_QBao_57131557.BusinessLogic.IService;
using IFDInternManagement_QBao_57131557.Models.UnitOfWork;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.Service
{
    public class StudentManagementService: IStudentManagementService
    {
        private readonly UnitOfWorkStudent _unitOfWork;
        public StudentManagementService()
        {
            _unitOfWork = new UnitOfWorkStudent();
        }
        public Student GetUserById(string id)
        {
            return _unitOfWork.UserRepository.GetByID(id);

        }
        public IEnumerable<Student> GetAllUsers()
        {
            return _unitOfWork.UserRepository.GetAll().ToList();
            
        }
        public int CreateUser(Student userEntity)
        {
            _unitOfWork.UserRepository.Insert(userEntity);
            _unitOfWork.Save();
            return userEntity.ID;

        }
        public bool UpdateUser(string id, Student userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.UserRepository.GetByID(id);
                    if (usr != null)
                    {
                        usr.RoleID = userEntity.RoleID;
                        usr.InternshipID = userEntity.InternshipID;
                        usr.ProjectID = userEntity.ProjectID;
                        usr.SpecializeID = userEntity.SpecializeID;
                        usr.Studentname = userEntity.Studentname;
                        _unitOfWork.UserRepository.Update(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public bool DeleteUser(string id)
        {
            var success = false;
            
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.UserRepository.GetByID(id);
                    if (usr != null)
                    {

                        _unitOfWork.UserRepository.Delete(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            
            return success;
        }
    }
}

