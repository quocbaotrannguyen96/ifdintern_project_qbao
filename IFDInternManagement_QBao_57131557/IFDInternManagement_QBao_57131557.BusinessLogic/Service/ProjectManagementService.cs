﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using IFDInternManagement_QBao_57131557.BusinessLogic.IService;
using IFDInternManagement_QBao_57131557.Models;
using IFDInternManagement_QBao_57131557.Models.UnitOfWork;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.Service
{
    public class ProjectManagementService : IProjectManagementService
    {
        private readonly UnitOfWorkProject _unitOfWork;
        public ProjectManagementService()
        {
            _unitOfWork = new UnitOfWorkProject();
        }
        public Project GetUserById(int id)
        {
            return _unitOfWork.UserRepository.GetByID(id);

        }
        public IEnumerable<Project> GetAllUsers()
        {
            return _unitOfWork.UserRepository.GetAll().ToList();
        }
        public int CreateUser(Project userEntity)
        {
            _unitOfWork.UserRepository.Insert(userEntity);
            _unitOfWork.Save();
            return userEntity.ProjectID;

        }
        public bool UpdateUser(int id, Project userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.UserRepository.GetByID(id);
                    if (usr != null)
                    {
                        usr.Projectname = userEntity.Projectname;
                        usr.Startdate = userEntity.Startdate;
                        usr.Enddate = userEntity.Enddate;
                        usr.ProjectID = userEntity.ProjectID;
                        _unitOfWork.UserRepository.Update(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public bool DeleteUser(int id)
        {
            var success = false;
            if (id > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.UserRepository.GetByID(id);
                    if (usr != null)
                    {

                        _unitOfWork.UserRepository.Delete(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}


