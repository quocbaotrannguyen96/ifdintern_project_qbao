﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using IFDInternManagement_QBao_57131557.Models;
using IFDInternManagement_QBao_57131557.Models.Repository;
using IFDInternManagement_QBao_57131557.Models.ViewModel;
using IFDInternManagement_QBao_57131557.BusinessLogic.IService;
using IFDInternManagement_QBao_57131557.Models.UnitOfWork;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.Service
{
    public class StudentCVManagementService : IStudentCVManagementService
    {
        private readonly UnitOfWorkStudentCV _unitOfWork;
        public StudentCVManagementService()
        {
            _unitOfWork = new UnitOfWorkStudentCV();
        }
        public StudentCV GetUserById(int id)
        {
            return _unitOfWork.UserRepository.GetByID(id);

        }
        public IEnumerable<StudentCV> GetAllUsers()
        {
            return _unitOfWork.UserRepository.GetAll().ToList();

        }
        public int CreateUser(StudentCV userEntity)
        {
            userEntity.Status = false;
            _unitOfWork.UserRepository.Insert(userEntity);
            _unitOfWork.Save();
            return userEntity.ID;

        }
        public bool UpdateUser(int id, StudentCV userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.UserRepository.GetByID(id);
                    if (usr != null)
                    {                        
                        usr.Status = userEntity.Status;
                        _unitOfWork.UserRepository.Update(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public bool DeleteUser(int id)
        {
            var success = false;

            using (var scope = new TransactionScope())
            {
                var usr = _unitOfWork.UserRepository.GetByID(id);
                if (usr != null)
                {

                    _unitOfWork.UserRepository.Delete(usr);
                    _unitOfWork.Save();
                    scope.Complete();
                    success = true;
                }
            }

            return success;
        }
    }
}


