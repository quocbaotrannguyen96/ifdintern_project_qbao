﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using IFDInternManagement_QBao_57131557.BusinessLogic.IService;
using IFDInternManagement_QBao_57131557.Models;
using IFDInternManagement_QBao_57131557.Models.UnitOfWork;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.Service
{
    public class ManagerManagementService : IManagerManagementService
    {
        private readonly UnitOfWorkManager _unitOfWork;
        public ManagerManagementService()
        {
            _unitOfWork = new UnitOfWorkManager();
        }
        public Manager GetUserById(int id)
        {
            return _unitOfWork.UserRepository.GetByID(id);

        }
        public IEnumerable<Manager> GetAllUsers()
        {
            return _unitOfWork.UserRepository.GetAll().ToList();
        }
        public int CreateUser(Manager userEntity)
        {
            _unitOfWork.UserRepository.Insert(userEntity);
            _unitOfWork.Save();
            return userEntity.ManagerID;

        }
        public bool UpdateUser(int id, Manager userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.UserRepository.GetByID(id);
                    if (usr != null)
                    {
                        usr.Managername = userEntity.Managername;
                        usr.Email = userEntity.Email;
                        usr.RoleID = userEntity.RoleID;
                        usr.ProjectID = userEntity.ProjectID;
                        _unitOfWork.UserRepository.Update(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public bool DeleteUser(int id)
        {
            var success = false;
            if (id > 0)
            {
                using (var scope = new TransactionScope())
                {
                    var usr = _unitOfWork.UserRepository.GetByID(id);
                    if (usr != null)
                    {

                        _unitOfWork.UserRepository.Delete(usr);
                        _unitOfWork.Save();
                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
    }
}

