﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.IService
{
    public interface ISpecializeManagementService
    {
        Specialize GetUserById(int id);
        IEnumerable<Specialize> GetAllUsers();
        int CreateUser(Specialize userEntity);
        bool UpdateUser(int id, Specialize userEntity);
        bool DeleteUser(int id);
    }
}
