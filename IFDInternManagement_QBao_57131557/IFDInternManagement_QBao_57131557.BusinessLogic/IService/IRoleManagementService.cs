﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.IService
{
    public interface IRoleManagementService
    {
        Role GetUserById(int id);
        IEnumerable<Role> GetAllUsers();
        int CreateUser(Role userEntity);
        bool UpdateUser(int id, Role userEntity);
        bool DeleteUser(int id);
    }
}
