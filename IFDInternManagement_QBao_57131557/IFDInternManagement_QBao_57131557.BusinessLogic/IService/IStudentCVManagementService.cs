﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.BusinessLogic.IService
{
    public interface IStudentCVManagementService
    {
        StudentCV GetUserById(int id);
        IEnumerable<StudentCV> GetAllUsers();
        int CreateUser(StudentCV userEntity);
        bool UpdateUser(int id, StudentCV userEntity);
        bool DeleteUser(int id);
    }
}
