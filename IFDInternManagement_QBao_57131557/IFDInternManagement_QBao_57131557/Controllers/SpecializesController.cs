﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.Controllers
{
    public class SpecializesController : Controller
    {
        private Model1 db = new Model1();

        // GET: Specializes
        public ActionResult Index()
        {
            return View(db.Specializes.ToList());
        }

        // GET: Specializes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Specialize specialize = db.Specializes.Find(id);
            if (specialize == null)
            {
                return HttpNotFound();
            }
            return View(specialize);
        }

        // GET: Specializes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Specializes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SpecializeID,Specializename,SpecializeContent")] Specialize specialize)
        {
            if (ModelState.IsValid)
            {
                db.Specializes.Add(specialize);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(specialize);
        }

        // GET: Specializes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Specialize specialize = db.Specializes.Find(id);
            if (specialize == null)
            {
                return HttpNotFound();
            }
            return View(specialize);
        }

        // POST: Specializes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SpecializeID,Specializename,SpecializeContent")] Specialize specialize)
        {
            if (ModelState.IsValid)
            {
                db.Entry(specialize).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(specialize);
        }

        // GET: Specializes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Specialize specialize = db.Specializes.Find(id);
            if (specialize == null)
            {
                return HttpNotFound();
            }
            return View(specialize);
        }

        // POST: Specializes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Specialize specialize = db.Specializes.Find(id);
            db.Specializes.Remove(specialize);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
