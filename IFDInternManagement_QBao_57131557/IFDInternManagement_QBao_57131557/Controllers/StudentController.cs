﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IFDInternManagement_QBao_57131557.BusinessLogic.IService;
using IFDInternManagement_QBao_57131557.BusinessLogic.Service;

using IFDInternManagement_QBao_57131557.Models;
using IFDInternManagement_QBao_57131557.Models.Repository;
using IFDInternManagement_QBao_57131557.Models.ViewModel;

namespace IFDInternManagement_QBao_57131557.Controllers
{
    public class StudentController : ApiController
    {
        private readonly IStudentManagementService _userServices;
        public StudentController(IStudentManagementService userManagementService)
        {
            _userServices = userManagementService;
        }

        // GET api/user
        public HttpResponseMessage Get()
        {
            var users = _userServices.GetAllUsers();
            if (users != null)
            {
                var userEntities = users as List<Student> ?? users.ToList();
                if (userEntities.Any())
                    return Request.CreateResponse(HttpStatusCode.OK, userEntities);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Users not found");
        }

        // GET api/user/5
        public HttpResponseMessage Get(string id)
        {
            var user = _userServices.GetUserById(id);
            if (user != null)
                return Request.CreateResponse(HttpStatusCode.OK, user);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "No user found for this id");
        }

        // POST api/user
        public int Post([FromBody] Student userEntity)
        {
            return _userServices.CreateUser(userEntity);
        }

        // PUT api/user/5
        public bool Put(string id, [FromBody]Student userEntity)
        {
            
                return _userServices.UpdateUser(id, userEntity);
            
           
        }

        // DELETE api/user/5
        public bool Delete(string id)
        {
            
                return _userServices.DeleteUser(id);
            
        }
    }
}
