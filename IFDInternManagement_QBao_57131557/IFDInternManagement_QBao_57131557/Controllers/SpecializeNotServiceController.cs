﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IFDInternManagement_QBao_57131557.Models.ViewModel;
using IFDInternManagement_QBao_57131557.Models;


namespace IFDInternManagement_QBao_57131557.Controllers
{
    public class SpecializeNotServiceController : ApiController
    {
        private IFDInternsEntities db = new IFDInternsEntities();
        public IQueryable<Specialize> GetSpecializes()
        {   
            return db.Specializes;
        }

        
    }
}
