﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IFDInternManagement_QBao_57131557.Models;
using Newtonsoft.Json;
using PagedList;

namespace IFDInternManagement_QBao_57131557.Controllers.GUI
{
    public class StudentCVMVCController : Controller
    {
        // GET: ManagerMVC
        private Model1 db = new Model1();
        string url = "http://localhost:49517/api/studentcv";
        HttpClient client;
        public StudentCVMVCController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // GET: mvcusers
        //public async Task<ActionResult> Index()
        //{
        //    HttpResponseMessage responseMessage = await client.GetAsync(url);
        //    if (responseMessage.IsSuccessStatusCode)
        //    {
        //        var responseData = responseMessage.Content.ReadAsStringAsync().Result;

        //        var usrs = JsonConvert.DeserializeObject<List<Manager>>(responseData);

        //        return View(usrs);
        //    }
        //    return View("Error");
        //}
        public ActionResult Index(int? page)
        {
            if (page == null) page = 1;

            var managers = (from l in db.StudentCVs
                            select l).OrderBy(x => x.ID);
            int pageSize = 5;

            int pageNumber = (page ?? 1);
            return View(managers.ToPagedList(pageNumber, pageSize));
        }
        // GET: mvcusers/Details/5
        public async Task<ActionResult> Details(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                var usr = JsonConvert.DeserializeObject<StudentCV>(responseData);
                return View(usr);
            }
            return View("Error");
        }
        // GET: mvcusers/Create
        public ActionResult Create()
        {
            StudentCV manager = new StudentCV();
            manager.KnowUCollection = db.KnowUs.ToList<KnowU>();
            manager.ExpectedCollection = db.ExpectedLocations.ToList<ExpectedLocation>();
            manager.SpecializeCollection = db.Specializes.ToList<Specialize>();
            return View(manager);
        }

        // POST: mvcusers/Create

        [HttpPost]
        public async Task<ActionResult> Create(StudentCV usr)
        {
            bool Status = false;
            string message = "";

            HttpResponseMessage responseMessage = await client.PostAsJsonAsync(url, usr);
            SendVerificationLinkEmail(usr.Email);
            message = "Đã gửi CV thành công!" +
                " thông báo đã được gửi đến địa chỉ email:" + usr.Email;
            Status = true;
            if (responseMessage.IsSuccessStatusCode)
            {
                ViewBag.Message = message;
                ViewBag.Status = Status;
                return View(usr);
                //return RedirectToAction("Index");
            }
            return RedirectToAction("Error");
        }


        // GET: mvcusers/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            //bool Status = false;
            //string message = "";            
            //StudentCV studentCV = new StudentCV();
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            //SendVerificationLinkEmail(studentCV.Email);            
            //Status = true;
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var manager = JsonConvert.DeserializeObject<StudentCV>(responseData);
                manager.KnowUCollection = db.KnowUs.ToList<KnowU>();
                manager.ExpectedCollection = db.ExpectedLocations.ToList<ExpectedLocation>();
                manager.SpecializeCollection = db.Specializes.ToList<Specialize>();
                SendVerificationLinkEmail(manager.Email, "Accept");
                return View(manager);
            }
            return View("Error");
        }

        // POST: mvcusers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<ActionResult> Edit(int id, StudentCV usr)
        {

            HttpResponseMessage responseMessage = await client.PutAsJsonAsync(url + "/" + id, usr);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Error");
        }

        public async Task<ActionResult> Delete(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var usr = JsonConvert.DeserializeObject<StudentCV>(responseData);

                return View(usr);
            }
            return View("Error");
        }
        //The DELETE method
        [HttpPost]
        public async Task<ActionResult> Delete(int id, StudentCV usr)
        {

            HttpResponseMessage responseMessage = await client.DeleteAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            return RedirectToAction("Error");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        [NonAction]
        public void SendVerificationLinkEmail(string email, string emailFor = "VerifyAccount")
        {
            var fromEmail = new MailAddress("mariobolobala@gmail.com", "mario bolobala");
            var toEmail = new MailAddress(email);
            var fromEmailPassword = "tranhuutich1";

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Chúng tôi đã nhận được CV của bạn!";
                body = "<br/><br/>Chúng tôi vui lòng thông báo CV của bạn" +
                    " đã được chúng tôi tiếp nhận. Vui lòng đợi quá trình xử lý hoàn tất!";

            }
            else if (emailFor == "Accept")
            {
                subject = "Accept CV";
                body = "<br/><br/>Chúng tôi vui lòng thông báo CV của bạn đã được chúng tôi chấp nhận. Vui lòng đến văn phòng INFOdation để phỏng vấn lúc";
            }
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }
    }
}

