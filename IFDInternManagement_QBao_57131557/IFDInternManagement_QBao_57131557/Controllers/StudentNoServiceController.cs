﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.Controllers
{
    public class StudentNoServiceController : Controller
    {
        // GET: StudentNoService
        public ActionResult Index(Student student)
        {
            IFDInternsEntities dbContext = new IFDInternsEntities();
            List<Student> students = dbContext.Students.Where(emp => emp.StudentID == student.StudentID).ToList();
            return View(students);
        }
        public ActionResult Details(Student std)
        {
            IFDInternsEntities dbContext = new IFDInternsEntities();
            Student student = dbContext.Students.Single(x => x.StudentID == std.StudentID);
            return View(student);
        }
    }
}