﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IFDInternManagement_QBao_57131557.Models;
using Newtonsoft.Json;

namespace IFDInternManagement_QBao_57131557.Controllers
{
    public class SpecializeMVCController : Controller
    {
        // GET: SpecializeMVC
        private IFDInternsEntities db = new IFDInternsEntities();
        string url = "http://localhost:49517/api/specialize";
        HttpClient client;
        public SpecializeMVCController()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // GET: mvcusers
        public async Task<ActionResult> Index()
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;

                var usrs = JsonConvert.DeserializeObject<List<Specialize>>(responseData);

                return View(usrs);
            }
            return View("Error");
        }

        // GET: mvcusers/Details/5
        public async Task<ActionResult> Details(int id)
        {
            HttpResponseMessage responseMessage = await client.GetAsync(url + "/" + id);
            if (responseMessage.IsSuccessStatusCode)
            {
                var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                var usr = JsonConvert.DeserializeObject<Specialize>(responseData);
                return View(usr);
            }
            return View("Error");
        }
    }
}