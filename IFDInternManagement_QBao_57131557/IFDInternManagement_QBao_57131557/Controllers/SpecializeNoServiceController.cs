﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.Controllers
{
    public class SpecializeNoServiceController : Controller
    {
        // GET: SpecializeNoService
        public ActionResult Index()
        {
            IFDInternsEntities dbContext = new IFDInternsEntities();
            List<Specialize> listSpecializes = dbContext.Specializes.ToList();
            return View(listSpecializes);
        }
        
    }
}