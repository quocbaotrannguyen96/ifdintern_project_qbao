﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using IFDInternManagement_QBao_57131557.Models;

namespace IFDInternManagement_QBao_57131557.Controllers
{
    public class StudentLoginController : Controller
    {
        //Registration Action
        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Exclude = "IsEmailVerified,ActivationCode")] Student student)
        {
            bool Status = false;
            string message = "";
            if (ModelState.IsValid)
            {
                var isExist = IsEmailExist(student.Email);
                if (isExist)
                {
                    ModelState.AddModelError("EmailExist", "Email already exist");
                    return View(student);
                }
                //Generate Activation Code
                student.ActivationCode = Guid.NewGuid();
                //Password Hashing
                student.Password = Crypto.Hash(student.Password);
                student.ConfirmPassword = Crypto.Hash(student.ConfirmPassword);
                student.IsEmailVerified = false;
                student.SpecializeID = 2;
                student.RoleID = 0;
                student.InternshipID = 0;
                student.ProjectID = 0;
                //Save to Database
                using (Model1 dc = new Model1())
                {
                    dc.Students.Add(student);
                    dc.SaveChanges();
                    //Send Email to User
                    SendVerificationLinkEmail(student.Email, student.ActivationCode.ToString());
                    message = "Đăng ký thành công! Link kích hoạt tài khoản " +
                        " đã được gửi đến địa chỉ email:" + student.Email;
                    Status = true;
                }
            }
            else
            {
                message = "Không hợp lệ!";
            }
            ViewBag.Message = message;
            ViewBag.Status = Status;
            return View(student);
        }

        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool Status = false;
            using (Model1 dc = new Model1())
            {
                dc.Configuration.ValidateOnSaveEnabled = false;
                var v = dc.Students.Where(a => a.ActivationCode == new Guid(id)).FirstOrDefault();
                if (v != null)
                {
                    v.IsEmailVerified = true;
                    dc.SaveChanges();
                    Status = true;
                }
                else
                {
                    ViewBag.Message = "Không hợp lệ!";
                }
            }
            ViewBag.Status = Status;
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(StudentLogin login, string ReturnUrl)
        {
            string message = "";
            using (Model1 dc = new Model1())
            {                
                    var v = dc.Students.Where(a => a.StudentID == login.StudentID).FirstOrDefault();
                    if (v != null)
                    {
                        if (string.Compare(Crypto.Hash(login.Password), v.Password) == 0)
                        {
                            int timeout = login.RememberMe ? 525000 : 20;
                            var ticket = new FormsAuthenticationTicket(login.StudentID, login.RememberMe, timeout);
                            string encrypted = FormsAuthentication.Encrypt(ticket);
                            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                            cookie.Expires = DateTime.Now.AddMinutes(timeout);
                            cookie.HttpOnly = true;
                            Response.Cookies.Add(cookie);


                            if (Url.IsLocalUrl(ReturnUrl))
                            {
                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {
                            message = "Thông tin không hợp lệ!";
                        }

                    }
                    else
                    {
                        message = "Thông tin không hợp lệ!";
                    }
                }
                ViewBag.Message = message;
                return View();
            }
        

        
        [HttpPost]
        public ActionResult Logout()
        {                        
            Session.Abandon();
            return RedirectToAction("Login", "StudentLogin");
        }
        [NonAction]
        public bool IsEmailExist(string email)
        {
            using (Model1 dc = new Model1())
            {
                var v = dc.Students.Where(a => a.Email == email).FirstOrDefault();
                return v != null;
            }
        }
        [NonAction]
        public void SendVerificationLinkEmail(string email, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyUrl = "/StudentLogin/" + emailFor + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("mariobolobala@gmail.com", "mario bolobala");
            var toEmail = new MailAddress(email);
            var fromEmailPassword = "tranhuutich1";

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Tài khoản của bạn đã được tạo. Vui lòng kích hoạt tại link kèm theo!";
                body = "<br/><br/>Chúng tôi vui lòng thông báo tài khoản của bạn" +
                    " đã được tạo thành công. Vui lòng click vào link bên dưới để xác nhận" +
                    //" <br/><br/><a href='" + link + "'>" + link + "</a> ";
                "<br/><br/><a href=" + link + ">Verify Account link</a>";
            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "<br/><br/>Chúng tôi nhận được thông báo yêu cầu cấp lại mật khẩu. Vui lòng click vào đường lên bên dưới để tiến hành tạo lại mật khẩu mới." +
                    "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }


            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgotPassword(string email)
        {
            string message = "";
            bool status = false;
            using (Model1 dc = new Model1())
            {
                var account = dc.Students.Where(a => a.Email == email).FirstOrDefault();
                if (account != null)
                {
                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.Email, resetCode, "ResetPassword");
                    account.ResetPasswordCode = resetCode;
                    dc.Configuration.ValidateOnSaveEnabled = false;
                    dc.SaveChanges();


                }
                else
                {
                    message = "Không tìm thấy tài khoản trùng khớp!!!";
                }
            }
            return View();
        }

        public ActionResult ResetPassword(string id)
        {
            using (Model1 dc = new Model1())
            {
                var user = dc.Students.Where(a => a.ResetPasswordCode == id).FirstOrDefault();
                if (user != null)
                {
                    ResetPassword model = new ResetPassword();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPassword model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                using (Model1 dc = new Model1())
                {
                    var user = dc.Students.Where(a => a.ResetPasswordCode == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        user.Password = Crypto.Hash(model.NewPassword);
                        user.ResetPasswordCode = "";
                        dc.Configuration.ValidateOnSaveEnabled = false;
                        dc.SaveChanges();
                        message = "Mật khẩu đã được tạo mới!";
                    }
                }
            }
            else
            {
                message = "Không hợp lệ!";
            }
            ViewBag.Message = message;
            return View(model);
        }
    }
}