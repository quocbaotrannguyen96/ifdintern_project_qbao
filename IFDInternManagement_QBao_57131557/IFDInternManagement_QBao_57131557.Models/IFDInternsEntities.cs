namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class IFDInternsEntities : DbContext
    {
        public IFDInternsEntities()
            : base("name=IFDInternsEntities")
        {
        }

        public virtual DbSet<Assessment> Assessments { get; set; }
        public virtual DbSet<ExpectedLocation> ExpectedLocations { get; set; }
        public virtual DbSet<Internship> Internships { get; set; }
        public virtual DbSet<KnowU> KnowUs { get; set; }
        public virtual DbSet<Manager> Managers { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Specialize> Specializes { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<StudentCV> StudentCVs { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<TrainingDetail> TrainingDetails { get; set; }
        public virtual DbSet<Trainingprogram> Trainingprograms { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Assessment>()
                .Property(e => e.AssessmentContent)
                .IsUnicode(false);

            modelBuilder.Entity<ExpectedLocation>()
                .HasMany(e => e.StudentCVs)
                .WithRequired(e => e.ExpectedLocation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Internship>()
                .Property(e => e.TrainingprogramID)
                .IsUnicode(false);

            modelBuilder.Entity<Internship>()
                .HasMany(e => e.Students)
                .WithRequired(e => e.Internship)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<KnowU>()
                .HasMany(e => e.StudentCVs)
                .WithRequired(e => e.KnowU)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Manager>()
                .Property(e => e.Email)
                .IsFixedLength();

            modelBuilder.Entity<Manager>()
                .Property(e => e.Password)
                .IsFixedLength();

            modelBuilder.Entity<Manager>()
                .HasMany(e => e.Internships)
                .WithRequired(e => e.Manager)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Managers)
                .WithRequired(e => e.Project)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Project>()
                .HasMany(e => e.Students)
                .WithRequired(e => e.Project)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Managers)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Students)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Specialize>()
                .HasMany(e => e.Students)
                .WithRequired(e => e.Specialize)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Specialize>()
                .HasMany(e => e.StudentCVs)
                .WithRequired(e => e.Specialize)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Student>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Student>()
                .Property(e => e.Phonenumber)
                .IsFixedLength();

            modelBuilder.Entity<Student>()
                .HasMany(e => e.TrainingDetails)
                .WithRequired(e => e.Student)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<StudentCV>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<StudentCV>()
                .Property(e => e.FrameworkUse)
                .IsUnicode(false);

            modelBuilder.Entity<TrainingDetail>()
                .Property(e => e.TrainingprogramID)
                .IsUnicode(false);

            modelBuilder.Entity<TrainingDetail>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<Trainingprogram>()
                .Property(e => e.TrainingprogramID)
                .IsUnicode(false);

            modelBuilder.Entity<Trainingprogram>()
                .Property(e => e.Trainingcontent)
                .IsUnicode(false);

            modelBuilder.Entity<Trainingprogram>()
                .HasMany(e => e.Internships)
                .WithRequired(e => e.Trainingprogram)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Trainingprogram>()
                .HasMany(e => e.TrainingDetails)
                .WithRequired(e => e.Trainingprogram)
                .WillCascadeOnDelete(false);
        }
    }
}
