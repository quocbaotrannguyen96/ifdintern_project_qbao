namespace IFDInternManagement_QBao_57131557.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditRole : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Manager", "RoleID", "dbo.Role");
            DropForeignKey("dbo.Student", "RoleID", "dbo.Role");
            DropPrimaryKey("dbo.Role");
            AlterColumn("dbo.Role", "RoleID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Role", "RoleID");
            AddForeignKey("dbo.Manager", "RoleID", "dbo.Role", "RoleID");
            AddForeignKey("dbo.Student", "RoleID", "dbo.Role", "RoleID");
            DropColumn("dbo.Role", "ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Role", "ID", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Student", "RoleID", "dbo.Role");
            DropForeignKey("dbo.Manager", "RoleID", "dbo.Role");
            DropPrimaryKey("dbo.Role");
            AlterColumn("dbo.Role", "RoleID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Role", "RoleID");
            AddForeignKey("dbo.Student", "RoleID", "dbo.Role", "RoleID");
            AddForeignKey("dbo.Manager", "RoleID", "dbo.Role", "RoleID");
        }
    }
}
