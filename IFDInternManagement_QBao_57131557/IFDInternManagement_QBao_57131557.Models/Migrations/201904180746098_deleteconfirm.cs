namespace IFDInternManagement_QBao_57131557.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deleteconfirm : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Student", "ConfirmPassword");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Student", "ConfirmPassword", c => c.String());
        }
    }
}
