namespace IFDInternManagement_QBao_57131557.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditIDStudentKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TrainingDetail", "ID", "dbo.Student");
            DropForeignKey("dbo.Assessment", "Student_ID", "dbo.Student");
            DropIndex("dbo.Assessment", new[] { "Student_ID" });
            DropColumn("dbo.Assessment", "StudentID");
            RenameColumn(table: "dbo.Assessment", name: "Student_ID", newName: "StudentID");
            DropPrimaryKey("dbo.Student");
            DropPrimaryKey("dbo.TrainingDetail");
            AddColumn("dbo.TrainingDetail", "Student_StudentID", c => c.String(maxLength: 15));
            AlterColumn("dbo.Assessment", "StudentID", c => c.String(maxLength: 15));
            AddPrimaryKey("dbo.Student", "StudentID");
            AddPrimaryKey("dbo.TrainingDetail", "TrainingprogramID");
            CreateIndex("dbo.Assessment", "StudentID");
            CreateIndex("dbo.TrainingDetail", "Student_StudentID");
            AddForeignKey("dbo.TrainingDetail", "Student_StudentID", "dbo.Student", "StudentID");
            AddForeignKey("dbo.Assessment", "StudentID", "dbo.Student", "StudentID");
            DropColumn("dbo.TrainingDetail", "StudentID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TrainingDetail", "StudentID", c => c.String(nullable: false, maxLength: 15));
            DropForeignKey("dbo.Assessment", "StudentID", "dbo.Student");
            DropForeignKey("dbo.TrainingDetail", "Student_StudentID", "dbo.Student");
            DropIndex("dbo.TrainingDetail", new[] { "Student_StudentID" });
            DropIndex("dbo.Assessment", new[] { "StudentID" });
            DropPrimaryKey("dbo.TrainingDetail");
            DropPrimaryKey("dbo.Student");
            AlterColumn("dbo.Assessment", "StudentID", c => c.Int());
            DropColumn("dbo.TrainingDetail", "Student_StudentID");
            AddPrimaryKey("dbo.TrainingDetail", new[] { "TrainingprogramID", "StudentID" });
            AddPrimaryKey("dbo.Student", "ID");
            RenameColumn(table: "dbo.Assessment", name: "StudentID", newName: "Student_ID");
            AddColumn("dbo.Assessment", "StudentID", c => c.String(maxLength: 15));
            CreateIndex("dbo.Assessment", "Student_ID");
            AddForeignKey("dbo.Assessment", "Student_ID", "dbo.Student", "ID");
            AddForeignKey("dbo.TrainingDetail", "ID", "dbo.Student", "ID");
        }
    }
}
