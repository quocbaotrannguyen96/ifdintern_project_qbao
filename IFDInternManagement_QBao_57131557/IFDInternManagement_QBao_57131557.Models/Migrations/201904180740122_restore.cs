namespace IFDInternManagement_QBao_57131557.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class restore : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.TrainingDetail", new[] { "Student_StudentID" });
            RenameColumn(table: "dbo.TrainingDetail", name: "Student_StudentID", newName: "StudentID");
            DropPrimaryKey("dbo.TrainingDetail");
            AlterColumn("dbo.TrainingDetail", "StudentID", c => c.String(nullable: false, maxLength: 15));
            AddPrimaryKey("dbo.TrainingDetail", new[] { "TrainingprogramID", "StudentID" });
            CreateIndex("dbo.TrainingDetail", "StudentID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.TrainingDetail", new[] { "StudentID" });
            DropPrimaryKey("dbo.TrainingDetail");
            AlterColumn("dbo.TrainingDetail", "StudentID", c => c.String(maxLength: 15));
            AddPrimaryKey("dbo.TrainingDetail", "TrainingprogramID");
            RenameColumn(table: "dbo.TrainingDetail", name: "StudentID", newName: "Student_StudentID");
            CreateIndex("dbo.TrainingDetail", "Student_StudentID");
        }
    }
}
