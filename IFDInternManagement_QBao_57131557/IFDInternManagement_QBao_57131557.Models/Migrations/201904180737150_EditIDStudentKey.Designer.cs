// <auto-generated />
namespace IFDInternManagement_QBao_57131557.Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class EditIDStudentKey : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(EditIDStudentKey));
        
        string IMigrationMetadata.Id
        {
            get { return "201904180737150_EditIDStudentKey"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
