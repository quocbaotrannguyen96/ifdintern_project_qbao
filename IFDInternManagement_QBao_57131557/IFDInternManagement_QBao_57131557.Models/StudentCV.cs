namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("StudentCV")]
    public partial class StudentCV
    {
        public int ID { get; set; }

        [Required]
        [StringLength(15)]
        public string StudentID { get; set; }

        [Required]
        [StringLength(254)]
        public string Email { get; set; }

        public int KnowUsID { get; set; }

        [Required]
        [StringLength(50)]
        public string FrameworkUse { get; set; }

        [Required]
        public string Problem { get; set; }

        [Required]
        public string ExAppAlone { get; set; }

        public int SpecializeID { get; set; }

        [Required]
        public string Intro { get; set; }

        public int ExpectedLocationID { get; set; }

        [Column(TypeName = "date")]
        public DateTime InternStartDate { get; set; }

        [Required]
        public string ExAppTeam { get; set; }

        public bool Status { get; set; }

        public virtual ExpectedLocation ExpectedLocation { get; set; }

        public virtual KnowU KnowU { get; set; }

        public virtual Specialize Specialize { get; set; }
        [NotMapped]
        public List<KnowU> KnowUCollection { get; set; }
        [NotMapped]
        public List<ExpectedLocation> ExpectedCollection { get; set; }
        [NotMapped]
        public List<Specialize> SpecializeCollection { get; set; }
    }
}
