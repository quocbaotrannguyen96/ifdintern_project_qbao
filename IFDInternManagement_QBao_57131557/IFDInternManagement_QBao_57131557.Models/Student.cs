namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Student")]
    public partial class Student
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Student()
        {
            Assessments = new HashSet<Assessment>();
            TrainingDetails = new HashSet<TrainingDetail>();
        }
        [Key]
        [StringLength(15)]
        public string StudentID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        

        [Required]
        [StringLength(50)]
        public string Studentname { get; set; }

        [Column(TypeName = "date")]
        public DateTime Birthday { get; set; }

        [Required]
        [StringLength(254)]
        public string Email { get; set; }

        [Required]
        [StringLength(100)]
        public string Address { get; set; }

        [Required]
        [StringLength(15)]
        public string Phonenumber { get; set; }

        public int RoleID { get; set; }

        [Required]
        [StringLength(50)]
        public string Unit { get; set; }

        [Required]
        public string Intro { get; set; }

        [Required]
        public string Password { get; set; }

        public int InternshipID { get; set; }

        public int ProjectID { get; set; }

        public int SpecializeID { get; set; }

        public bool IsEmailVerified { get; set; }

        public Guid ActivationCode { get; set; }

        [StringLength(100)]
        public string ResetPasswordCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Assessment> Assessments { get; set; }

        public virtual Internship Internship { get; set; }

        public virtual Project Project { get; set; }

        public virtual Role Role { get; set; }

        public virtual Specialize Specialize { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TrainingDetail> TrainingDetails { get; set; }
        [NotMapped]
        public List<Role> RoleCollection { get; set; }
        [NotMapped]
        public List<Project> ProjectCollection { get; set; }
        [NotMapped]
        public List<Internship> InternshipCollection { get; set; }
        [NotMapped]
        public List<Specialize> SpecializeCollection { get; set; }
    }
}
