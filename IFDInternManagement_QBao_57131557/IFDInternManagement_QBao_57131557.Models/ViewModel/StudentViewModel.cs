﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFDInternManagement_QBao_57131557.Models.ViewModel
{
    public class StudentViewModel
    {
        public int ID { get; set; }
        public string StudentID { get; set; }
        public string Studentname { get; set; }
        public System.DateTime Birthday { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phonenumber { get; set; }
        public int RoleID { get; set; }
        public string Unit { get; set; }
        public string Intro { get; set; }
        public string Password { get; set; }
        public int InternshipID { get; set; }
        public int ProjectID { get; set; }
        public int SpecializeID { get; set; }
        public bool IsEmailVerified { get; set; }
        public System.Guid ActivationCode { get; set; }
        public string ResetPasswordCode { get; set; }

        public virtual ICollection<Assessment> Assessments { get; set; }
        public virtual Internship Internship { get; set; }
        public virtual Project Project { get; set; }
        
        public virtual Role Role { get; set; }
        public virtual Specialize Specialize { get; set; }
        public virtual ICollection<TrainingDetail> TrainingDetails { get; set; }
    }
}
