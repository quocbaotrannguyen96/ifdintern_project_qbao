﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFDInternManagement_QBao_57131557.Models.ViewModel
{
    public class SpecializeViewModel
    {
        public int ID { get; set; }
        public int SpecializeID { get; set; }
        public string Specializename { get; set; }
        public string SpecializeContent { get; set; }

        //public ICollection<StudentViewModel> Students { get; set; }
    }
}
