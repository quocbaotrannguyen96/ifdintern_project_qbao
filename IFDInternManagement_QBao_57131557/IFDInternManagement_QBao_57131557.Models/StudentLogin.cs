﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFDInternManagement_QBao_57131557.Models
{
    public class StudentLogin
    {
        [Display(Name = "Mã Tài Khoản")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        public string StudentID { get; set; }

        [Display(Name = "Mật khẩu")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Không được bỏ trống!")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Ghi nhớ tài khoản")]
        public bool RememberMe { get; set; }
    }
}
