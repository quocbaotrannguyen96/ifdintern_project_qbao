namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Assessment")]
    public partial class Assessment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AssessmentID { get; set; }

        [StringLength(15)]
        public string StudentID { get; set; }

        public bool? Pass { get; set; }

        [Column(TypeName = "text")]
        public string AssessmentContent { get; set; }

        public virtual Student Student { get; set; }
    }
}
