namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Specialize")]
    public partial class Specialize
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Specialize()
        {
            this.Students = new HashSet<Student>();
            this.StudentCVs = new HashSet<StudentCV>();
        }
        [Key]
        public int SpecializeID { get; set; }

        [Required]
        [StringLength(50)]
        public string Specializename { get; set; }

        [Required]
        [StringLength(150)]
        public string SpecializeContent { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Student> Students { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentCV> StudentCVs { get; set; }
    }
}
