namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Manager")]
    public partial class Manager
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Manager()
        {
            Internships = new HashSet<Internship>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ManagerID { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Managername { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        public int RoleID { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }

        public int ProjectID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Internship> Internships { get; set; }

        public virtual Project Project { get; set; }

        public virtual Role Role { get; set; }
        [NotMapped]
        public List<Role> RoleCollection { get; set; }
        [NotMapped]
        public List<Project> ProjectCollection { get; set; }
    }
}
