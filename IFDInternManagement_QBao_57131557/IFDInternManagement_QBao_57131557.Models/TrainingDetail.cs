namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TrainingDetail")]
    public partial class TrainingDetail
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(15)]
        public string TrainingprogramID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(15)]
        public string StudentID { get; set; }

        [Column(TypeName = "text")]
        public string Content { get; set; }

        public virtual Student Student { get; set; }

        public virtual Trainingprogram Trainingprogram { get; set; }
    }
}
