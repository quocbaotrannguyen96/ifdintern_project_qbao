﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFDInternManagement_QBao_57131557.Models
{
    public class IFDDbContext: DbContext
    {
        public IFDDbContext(): base("test")
        {

        }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Role> Roles { get; set; }

    }
}
