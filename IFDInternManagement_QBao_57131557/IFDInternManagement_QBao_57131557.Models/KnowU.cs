namespace IFDInternManagement_QBao_57131557.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class KnowU
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KnowU()
        {
            StudentCVs = new HashSet<StudentCV>();
        }

        [Key]
        public int KnowUsID { get; set; }

        [StringLength(150)]
        public string KnowUsName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentCV> StudentCVs { get; set; }
    }
}
